using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private Ball ball;

    int score = 0;

    #region unity lifecycle

    private void OnCollisionEnter2D()
    {
        score++;
        scoreText.text = "<align=\"center\">" + score;
        ball.ResetPosition();
    }

    #endregion

}
