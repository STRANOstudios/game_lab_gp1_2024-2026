using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    [SerializeField] float BallSpeed;
    [SerializeField] Rigidbody2D rb; 

    #region my method

    public void StartMotion()
    {
        rb.velocity = Vector2.right * BallSpeed;
    }
    public void ResetPosition()
    {
        transform.position = Vector2.zero;
        StartMotion();
    }

    #endregion

}
