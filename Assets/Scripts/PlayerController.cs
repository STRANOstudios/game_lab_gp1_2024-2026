using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] float MaxDeltaMove = 4f;

    [SerializeField] Bar player1;
    [SerializeField] Bar player2;
    [SerializeField] Ball ball;

    float p1Move, p2Move;

    #region unity method

    void Start()
    {
        player1 = GameObject.Find("Player1").GetComponent<Bar>();
        player2 = GameObject.Find("Player2").GetComponent<Bar>();

        if (ball != null) ball.StartMotion();
    }
    void Update()
    {
        p1Move = Input.GetAxis("Player1Input");
        p2Move = Input.GetAxis("Player2Input");
    }
    void FixedUpdate()
    {
        player1.Move(p1Move, MaxDeltaMove);
        player2.Move(p2Move, MaxDeltaMove);
    }

    #endregion

    #region my method



    #endregion

}
