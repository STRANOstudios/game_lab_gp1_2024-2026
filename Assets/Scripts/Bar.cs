using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    [SerializeField] float playerSpeed = 5f;
    Vector2 oldPosition;
    public void Move(float axisValue, float maxDeltaMove)
    {
        transform.Translate(Vector2.up * axisValue * Time.deltaTime * playerSpeed);
        if (Mathf.Abs(transform.position.y) > maxDeltaMove) transform.position = oldPosition;
        oldPosition = transform.position;
    }
}
